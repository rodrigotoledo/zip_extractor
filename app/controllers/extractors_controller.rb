class ExtractorsController < ApplicationController
  before_action :set_extractor, only: [:show, :edit, :update, :destroy]

  # GET /extractors
  # GET /extractors.json
  def index
    @extractors = Extractor.all
  end

  # GET /extractors/1
  # GET /extractors/1.json
  def show
  end

  # GET /extractors/new
  def new
    @extractor = Extractor.new
  end

  # GET /extractors/1/edit
  def edit
  end

  # POST /extractors
  # POST /extractors.json
  def create
    @extractor = Extractor.new(extractor_params)

    respond_to do |format|
      if @extractor.save
        format.html { redirect_to @extractor, notice: 'Extractor was successfully created.' }
        format.json { render :show, status: :created, location: @extractor }
      else
        format.html { render :new }
        format.json { render json: @extractor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /extractors/1
  # PATCH/PUT /extractors/1.json
  def update
    respond_to do |format|
      if @extractor.update(extractor_params)
        format.html { redirect_to @extractor, notice: 'Extractor was successfully updated.' }
        format.json { render :show, status: :ok, location: @extractor }
      else
        format.html { render :edit }
        format.json { render json: @extractor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /extractors/1
  # DELETE /extractors/1.json
  def destroy
    @extractor.destroy
    respond_to do |format|
      format.html { redirect_to extractors_url, notice: 'Extractor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_extractor
      @extractor = Extractor.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def extractor_params
      params.require(:extractor).permit(:title, :description, :is_extracted)
    end
end
