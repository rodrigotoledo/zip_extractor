json.extract! extractor, :id, :title, :description, :is_extracted, :created_at, :updated_at
json.url extractor_url(extractor, format: :json)
