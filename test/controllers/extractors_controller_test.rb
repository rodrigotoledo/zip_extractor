require 'test_helper'

class ExtractorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @extractor = extractors(:one)
  end

  test "should get index" do
    get extractors_url
    assert_response :success
  end

  test "should get new" do
    get new_extractor_url
    assert_response :success
  end

  test "should create extractor" do
    assert_difference('Extractor.count') do
      post extractors_url, params: { extractor: { description: @extractor.description, is_extracted: @extractor.is_extracted, title: @extractor.title } }
    end

    assert_redirected_to extractor_url(Extractor.last)
  end

  test "should show extractor" do
    get extractor_url(@extractor)
    assert_response :success
  end

  test "should get edit" do
    get edit_extractor_url(@extractor)
    assert_response :success
  end

  test "should update extractor" do
    patch extractor_url(@extractor), params: { extractor: { description: @extractor.description, is_extracted: @extractor.is_extracted, title: @extractor.title } }
    assert_redirected_to extractor_url(@extractor)
  end

  test "should destroy extractor" do
    assert_difference('Extractor.count', -1) do
      delete extractor_url(@extractor)
    end

    assert_redirected_to extractors_url
  end
end
