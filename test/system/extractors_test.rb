require "application_system_test_case"

class ExtractorsTest < ApplicationSystemTestCase
  setup do
    @extractor = extractors(:one)
  end

  test "visiting the index" do
    visit extractors_url
    assert_selector "h1", text: "Extractors"
  end

  test "creating a Extractor" do
    visit extractors_url
    click_on "New Extractor"

    fill_in "Description", with: @extractor.description
    check "Is extracted" if @extractor.is_extracted
    fill_in "Title", with: @extractor.title
    click_on "Create Extractor"

    assert_text "Extractor was successfully created"
    click_on "Back"
  end

  test "updating a Extractor" do
    visit extractors_url
    click_on "Edit", match: :first

    fill_in "Description", with: @extractor.description
    check "Is extracted" if @extractor.is_extracted
    fill_in "Title", with: @extractor.title
    click_on "Update Extractor"

    assert_text "Extractor was successfully updated"
    click_on "Back"
  end

  test "destroying a Extractor" do
    visit extractors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Extractor was successfully destroyed"
  end
end
