class CreateExtractors < ActiveRecord::Migration[6.0]
  def change
    create_table :extractors do |t|
      t.string :title
      t.text :description
      t.boolean :is_extracted, default: false

      t.timestamps
    end
  end
end
